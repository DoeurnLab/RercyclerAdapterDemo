package com.example.doeurn.recyclerviewdemo.adapter;

import android.content.Context;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.doeurn.recyclerviewdemo.R;
import com.example.doeurn.recyclerviewdemo.callback.AdapterCallBack;
import com.example.doeurn.recyclerviewdemo.entity.Song;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by Doeurn on 11/10/17.
 */

public class SongAdapter  extends RecyclerView.Adapter<SongAdapter.ViewHolder>{


    List<Song> list;
    AdapterCallBack callBack;
    Context context;
    public  SongAdapter(Context context,List<Song> list){
        this.context=context;
        this.list=list;
        callBack = (AdapterCallBack) context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view =LayoutInflater.from(context).inflate(
                R.layout.item_layout,
                parent,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.songImage.setImageResource(list.get(position).getImage());
        holder.tvtitle.setText(list.get(position).getTitle());
        holder.tvSinger.setText(list.get(position).getSinger());


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        private ImageView songImage;
        private TextView tvtitle;
        private TextView tvSinger;
        private ImageButton btnDelete;
        public ViewHolder(View itemView) {
            super(itemView);
            songImage= (ImageView) itemView.findViewById(R.id.imageAlbume);
            tvtitle= (TextView) itemView.findViewById(R.id.textView);
            tvSinger= (TextView) itemView.findViewById(R.id.tvSinger);
            btnDelete= (ImageButton) itemView.findViewById(R.id.imageButton);

            btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e("click -> " , "deleted");
                    Song song=list.get(getAdapterPosition());
                    callBack.deleteItem(song,getAdapterPosition());
                }
            });
        }
    }

}


