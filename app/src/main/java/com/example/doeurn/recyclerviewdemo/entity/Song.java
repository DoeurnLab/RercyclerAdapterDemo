package com.example.doeurn.recyclerviewdemo.entity;

/**
 * Created by Doeurn on 11/10/17.
 */

public class Song {
    private String title;
    private String singer;
    private int image;

    public Song(String title, String singer, int image) {
        this.title = title;
        this.singer = singer;
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSinger() {
        return singer;
    }

    public void setSinger(String singer) {
        this.singer = singer;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Song{" +
                "title='" + title + '\'' +
                ", singer='" + singer + '\'' +
                ", image=" + image +
                '}';
    }
}
