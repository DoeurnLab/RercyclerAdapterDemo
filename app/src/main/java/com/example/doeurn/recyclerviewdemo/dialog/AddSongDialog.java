package com.example.doeurn.recyclerviewdemo.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.doeurn.recyclerviewdemo.R;
import com.example.doeurn.recyclerviewdemo.callback.AdapterCallBack;
import com.example.doeurn.recyclerviewdemo.entity.Song;

/**
 * Created by Asus on 11/10/2017.
 */

public class AddSongDialog  extends DialogFragment{

    AdapterCallBack.DialogCallBack callBack;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        callBack= (AdapterCallBack.DialogCallBack) context;
    }

    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {
        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
        builder.setTitle("Add new Song");
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.add_diglog_layout,null);
        final ViewHolder viewHolder= new ViewHolder(view);
        builder.setView(view);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Song song=new Song(viewHolder.edSongTitle.getText().toString(),
                        viewHolder.edSinger.getText().toString(),
                        R.mipmap.ic_launcher_round);
                Log.e("Song -> " , song.toString());

                callBack.getSong(song);

            }
        });


        return builder.create();
    }

    class ViewHolder {
        private EditText edSongTitle;
        private EditText edSinger;
        public ViewHolder(View view){
            edSongTitle= (EditText) view.findViewById(R.id.tv_add_song);
            edSinger= (EditText) view.findViewById(R.id.tv_add_singer);
        }
    }
}
