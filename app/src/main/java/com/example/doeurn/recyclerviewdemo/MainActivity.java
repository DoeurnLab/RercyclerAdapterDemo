package com.example.doeurn.recyclerviewdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.example.doeurn.recyclerviewdemo.adapter.SongAdapter;
import com.example.doeurn.recyclerviewdemo.callback.AdapterCallBack;
import com.example.doeurn.recyclerviewdemo.dialog.AddSongDialog;
import com.example.doeurn.recyclerviewdemo.entity.Song;

import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.recyclerview.adapters.SlideInBottomAnimationAdapter;

public class MainActivity extends AppCompatActivity implements AdapterCallBack,
        AdapterCallBack.DialogCallBack{

    RecyclerView recyclerView;
    List<Song> list;

    SongAdapter songAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        list=new ArrayList<>();
        recyclerView= (RecyclerView) findViewById(R.id.recyclerView);
        //create Adapter
        songAdapter=new SongAdapter(this,list);

        SlideInBottomAnimationAdapter sideIn=new SlideInBottomAnimationAdapter(songAdapter);
        sideIn.setDuration(500);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(sideIn);

        getSong();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater =getMenuInflater();
        inflater.inflate(R.menu.option_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case  R.id.addNewItem:{
                AddSongDialog addSongDialog=new AddSongDialog();
                addSongDialog.show(getFragmentManager(),"add song dialog");
                return true;
            }
        }
        return false;
    }

    private void getSong(){
        for(int i=0;i<=100;i++){
            list.add(new Song("title "+i,"singer "+i,R.mipmap.ic_launcher_round));
        }
    }

    @Override
    public void deleteItem(Song song ,int pos) {
        deleteSong(song, pos);
        Log.e("song pos",pos +"");
    }

    public void deleteSong(Song song, int pos){

        list.remove(song);
        songAdapter.notifyItemRemoved(pos);

    }

    @Override
    public void getSong(Song song) {
        addNewSong(song);
    }

    private void addNewSong(Song song){
        list.add(0,song);
        songAdapter.notifyItemInserted(0);
    }
}
