package com.example.doeurn.recyclerviewdemo.callback;

import com.example.doeurn.recyclerviewdemo.entity.Song;

/**
 * Created by Asus on 11/10/2017.
 */

public interface AdapterCallBack {
    void deleteItem(Song song , int position);

    interface DialogCallBack{
        void getSong(Song song);
    }
}
